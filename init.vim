" set runtimepath^=~/.vim runtimepath+=~/.vim/after
" let &packpath = &runtimepath

set nocompatible              " be iMproved, required
filetype plugin on            " required
set encoding=UTF-8

" Vim Plugins "--------------------------------------------------------------{{{

"Detect if Vundle is installed {{{
let s:vundleloc = $HOME.'/.local/share/nvim/site/autoload/plug.vim' 
if filereadable(s:vundleloc) == v:false
    let s:yes = inputdialog("No plug.vim detected for " . $USER . "\nWould you like to install it? [y/n]\n\r ")
    if s:yes == "y"
        execute "!curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
        echo "\nYou are now plugged"
    else
        echo "\nOk, your loss."
    endif
endif
"}}}

call plug#begin($HOME.'/.local/share/nvim/plugged')

" ==============================================================================
"  Extensions
" ==========================================================================={{{
Plug 'scrooloose/nerdtree'                        " Project and file navigation
Plug 'Xuyuanp/nerdtree-git-plugin'                " NerdTree git functionality
Plug 'ryanoasis/vim-devicons'                     " Dev Icons, symbols for nerdtree and other cli stuff
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'    " Color nerd symbols, needs vim-devicons
Plug 'spolu/dwm.vim'                              " Vim natural window management, inspired in dwm
Plug 'thaerkh/vim-indentguides'                   " Visual representation of indents
Plug 'tpope/vim-commentary'                       " Comment lines with gc
Plug 'tpope/vim-fugitive'                         " ? Git interface?
Plug 'tpope/vim-surround'                         " Parentheses, brackets, quotes, XML tags, and more
Plug 'tpope/vim-ragtag'                           " Mappings for HTML, XML, and tags
Plug 'tpope/vim-unimpaired'                       " Interesting common bindings, check git for all
Plug 'vim-scripts/matchit.zip'                    " Extended '%'-lookup for HMTL, Latex and stuff
" ===========================================================================}}}
"  Colorschemes
" ==========================================================================={{{
Plug 'flazz/vim-colorschemes'                     " Colorschemes
" Plug 'christophermca/meta5'                       " meta5: tronlike cholorscheme
" ===========================================================================}}}
"  Language extensions
" ==========================================================================={{{
Plug 'python-mode/python-mode'                    " Python stuff
Plug 'rust-lang/rust.vim'                         " Rust stuff
" Plug 'rudrab/vimf90', {'for' : 'f90'}             " Fortran 90 syntax
" Plug 'stevearc/vim-arduino'                       " Arduino
Plug 'cespare/vim-toml'                           " TOML syntax
Plug 'mattn/emmet-vim'                            " HTML God Mode
" ===========================================================================}}}
"  Language Server Protocol extensions
" ==========================================================================={{{
"Plug 'prabirshrestha/async.vim'                   " This is needed for vim-lsp
" Plug 'prabirshrestha/vim-lsp'                     " Language Server Protocol
" *-----------------------------------------------------------------------------
"                                                   " Another LSP, requires manual install
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
" ===========================================================================}}}
"  Autocompletion and snippets
" ==========================================================================={{{
Plug 'Valloric/YouCompleteMe'                     " Shit just got REAL
Plug 'SirVer/ultisnips'                            " Vim snippets. Tracks the engine.
Plug 'honza/vim-snippets'                          " Snippets are separated from the engine. Add this if you want them
" ===========================================================================}}}
"  Others
" ==========================================================================={{{
Plug 'neomake/neomake'                            " Asynchonous program running
Plug 'coddingtonbear/neomake-platformio'          " Integrate platformio using neomake
" ===========================================================================}}}

" All of your Plugs must be added before the following line
call plug#end()            " required
" }}}

" Mappings and misc ---------------------------------------------------------{{{
"
colorscheme meta5 "meta5 desert256 elflord meta5
colorscheme OceanicNext

syntax on
filetype plugin indent on
highlight ColorColumn ctermbg=235 guibg=#2c2d27
hi Normal ctermbg=none
hi NonText ctermbg=none

set nowrap

set expandtab
set tabstop=4
set shiftwidth=4
set textwidth=80
" set cursorline                              " shows line under the cursor's line
" highlight CursorLine ctermbg=135 guibg=#5c2d27

set showmatch                               " shows matching part of bracket pairs (), [], {}

" set guifont=mononoki\ Symbols\ Nerd\ Font\ 16
set guifont=DejaVu\ Sans\ Mono\ 13
" Inverts <j> with <k> navigation. YEAH, THE STANDARD SUCKS, COME AT ME BRO.
noremap j k
noremap k j
" Activates highlight for searches
set hlsearch 
"Deactivates hl for searches
" nnoremap <CR> :nohlsearch<CR><CR>
nnoremap <Esc><Esc> :nohlsearch<CR><CR>
" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %
" Smart Home
noremap <expr> <silent> <Home> col('.') == match(getline('.'),'\S')+1 ? '0' : '^'
imap <silent> <Home> <C-O><Home>
" Save and suspend
:noremap <C-Z> :w<CR><C-z> 
" End of line and enter
:noremap s $A<CR><Esc>
" Colorize column 80
let &colorcolumn=join(range(81,81),",")
set previewheight=60
" Open tag in vsplit
" nnoremap <C-]> :execute "vertical ptag " . expand("<cword>")<CR>

" Leader
let mapleader = "\<Space>"

" Enable folding
set foldmethod=indent
" Start unfolded
set foldlevelstart=99
" binds 'shift tab' to 'toggle fold'
nnoremap <s-tab> za

nnoremap <silent> <Leader>r :call mappings#cycle_numbering()<CR>

nnoremap <Leader>k :tabnext<CR>
nnoremap <Leader>j :tabprev<CR>
nnoremap <Leader>l :tablast<CR>
nnoremap <Leader>h :tabfirst<CR>
nnoremap <Leader>n :tabnew<space>

noremap ññ :%s///g<Left><Left><Left>
noremap ñ/ :%s///cg<Left><Left><Left><Left>
noremap <M-l> 20zl  " map?
noremap <M-h> 20zh
inoremap <M-l> <right>
inoremap <M-h> <left>

" Terminal ---------------------- {{{
nnoremap <Leader>T :tabnew<CR>:terminal<CR>
nnoremap <Leader>t :7split<CR>:terminal<CR>
tnoremap <Esc> <C-\><C-n>
" }}}

" Default: C-u deletes line to beginng, like in a terminal
" Override: C-u uppercases current word
imap <C-u> <esc>bveUea

" Default: Don't know, don't care
" Override: Allows going up the history without the arrow keys
cnoremap <C-j> <up>
cnoremap <C-k> <down>

augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldlevelstart=0
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" }}}

" Plugin Settings {{{
"=====================================================
" vim-commentary
"=================================================={{{
nnoremap <Leader>c Ygcp
"==================================================}}}
" dwm.vim
"=================================================={{{
let g:dwm_map_keys=0
let g:dwm_master_pane_width=82
nnoremap <C-J> <C-W>W
nnoremap <C-K> <C-W>w
nmap <C-N> <Plug>DWMNew
nmap <C-C> <Plug>DWMClose
nmap <C-@> <Plug>DWMFocus
nmap <C-Space> <Plug>DWMFocus
nmap <C-L> <Plug>DWMGrowMaster
nmap <C-H> <Plug>DWMShrinkMaster
"==================================================}}}
" YouCompleteMe Bindings
"=================================================={{{
noremap <C-a>d :YcmCompleter GetDoc<CR>
noremap <C-a>s :YcmCompleter GoTo<CR>
noremap <C-a>c :YcmCompleter GoToDefinition<CR>
noremap <C-a>C :YcmCompleter GoToImplementation<CR>
let g:ycm_show_diagnostics_ui=0

" YouCompleteMe and UltiSnips compatibility.
let g:UltiSnipsExpandTrigger = '<Tab>'
let g:UltiSnipsJumpForwardTrigger = '<Tab>'
let g:UltiSnipsJumpBackwardTrigger = '<S-Tab>'

" Prevent UltiSnips from removing our carefully-crafted mappings.
let g:UltiSnipsMappingsToIgnore = ['autocomplete']

let g:ycm_key_list_select_completion = ['<C-k>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-j>', '<Up>']

" Additional UltiSnips config.
let g:UltiSnipsSnippetsDir = '~/.config/nvim/ultisnips'
let g:UltiSnipsSnippetDirectories = ['ultisnips']

" Additional YouCompleteMe config.
let g:ycm_complete_in_comments = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_seed_identifiers_with_syntax = 1

let g:ycm_semantic_triggers = {
      \   'haskell': [
      \     '.',
      \     '(',
      \     ',',
      \     ', '
      \   ]
      \ }

" Same as default, but with "markdown" and "text" removed.
let g:ycm_filetype_blacklist = {
      \   'notes': 1,
      \   'unite': 1,
      \   'tagbar': 1,
      \   'pandoc': 1,
      \   'qf': 1,
      \   'vimwiki': 1,
      \   'infolog': 1,
      \   'mail': 1
      \ }


"==================================================}}}
"" Indent Guides Settings 
"=================================================={{{
set listchars=tab:›\ ,trail:•,extends:#,nbsp:.

"==================================================}}}
"" NERDTree settings
"=================================================={{{
let NERDTreeIgnore=['\.pyc$', '\.pyo$', '__pycache__$']     " Ignore files in NERDTree
let NERDTreeWinSize=40
autocmd VimEnter * if !argc() | NERDTree | endif  " Load NERDTree only if vim is run without arguments
nmap ° :NERDTreeToggle<CR>

"==================================================}}}
"" LanguageClient-neovim Language Server Protocol
"=================================================={{{
" Rust : needs package rustup, and installation of toolchain + RLS
" Python : needs package python-language-server
" C++ : needs cquery package (AUR), check
"   https://github.com/autozimu/LanguageClient-neovim/wiki/cquery
"   https://github.com/cquery-project/cquery/wiki/Vim
let g:LanguageClient_serverCommands = {
            \ 'rust': ['/usr/bin/rustup', 'run', 'stable', 'rls'],
            \ 'python': ['pyls'],
            \ 'cpp': ['/usr/bin/cquery','--language-server', '--log-file=/tmp/cq_cpp.log', '--record=/tmp/cq_cpp_io', '--init={"cacheDirectory":"'.$HOME.'/Other/cquery/cpp"}'],
            \ 'c': ['/usr/bin/cquery', '--language-server', '--log-file=/tmp/cq_c.log', '--init={"cacheDirectory":"'.$HOME.'/Other/cquery/c"}'],
            \ 'r': ['R', '--quiet', '--slave', '-e', 'languageserver::run()'],
            \ }

nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
nnoremap <silent> <F3> :call LanguageClient#textDocument_references()<CR>
nnoremap <silent> <F4> :call LanguageClient#textDocument_implementation()<CR>
nnoremap <silent> <F6> :call LanguageClient_contextMenu()<CR>
nnoremap <silent> <F12> :LanguageClientStop<CR>:sleep 200ms<CR>:LanguageClientStart<CR>

let g:LanguageClient_loadSettings = 1
let g:LanguageClient_diagnosticsEnable = 1
" let g:ycm_extra_conf_globlist = ['~/FuseFS.storage/catkin_workspace/src/*','!~/*']
" }}}
" }}}

" My functions (and bindings to them) -------------------- {{{
" source $HOME/.config/nvim/delinhere.vim         " Two functions to quickly delte inside ([{
nnoremap dih  :call DeleteInHere()<CR>
nnoremap cih  :call ChangeInHere()<CR>
nnoremap yih  :call YankInHere()<CR>
" }}}

" Custom language bindings {{{
autocmd FileType python source $HOME/.config/nvim/lang_plugin/py.vim
autocmd FileType rust source $HOME/.config/nvim/lang_plugin/rs.vim
autocmd FileType r source $HOME/.config/nvim/lang_plugin/R.vim
autocmd FileType cpp source $HOME/.config/nvim/lang_plugin/cpp.vim
" }}}
